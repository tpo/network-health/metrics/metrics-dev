# Developing on CollecTor

There are a few part of collector that might not work in the container. For
example on the prodution collector instance archives of bridges data is received
from bridgedb.

These files are sync-ed in:
/srv/collector.torproject.org/collector/in/bridge-descriptors/polyanthum/

Without this files, collector won't parse the bridge descriptors.
