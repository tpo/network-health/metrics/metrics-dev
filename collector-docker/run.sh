#!/bin/sh
#
#####  Please edit the following variables.
#
# Version of the jar to use
COLVERSION="latest"
#
# Directory for runtime.
COLDIR="/srv/collector.torproject.org/collector"
#
# Start command, which uses the above version variable.
COLSTARTCMD="java -Xmx8g -cp .:collector-$COLVERSION.jar org.torproject.metrics.collector.Main collector.properties"

echo "Starting CollecTor"
cd ${COLDIR};
${COLSTARTCMD}
